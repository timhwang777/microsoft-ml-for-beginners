# Microsoft Machine Learning for Beginners Programming Solutions

## Table of Contents
1. [About the Project](#about-the-project)
2. [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
3. [Author](#author)

## About the Project
This repository contains my practices and solutions for Microsoft's [ML-for-Beginners](https://github.com/microsoft/ML-For-Beginners) course on GitHub. Some solutions may be outdated due to software updates or deprecation. I noticed there are some outdated Python packages and function methods, especially in lectures eight and nine. Be aware when practicing on your own.

Please note that this is only a practice project, and I do not guarantee the authenticity of the solutions.

If you have any questions, please refer to the original repository to submit a request.

## Getting Started
Clone the original repository.
```bash
git clone https://github.com/microsoft/ML-For-Beginners.git
``` 
### Prerequisites
The following tools are required:

1. Python 3
2. Jupyter Notebook and its associated tools
3. Conda (recommended)

## Author
Timothy Hwang